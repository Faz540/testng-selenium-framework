# Example Frontend Automation using Selenium (Java) - TestNG Framework:

#### Author: 
Paul Joseph Farrell
#### Email: 
pauljosephfarrell89@gmail.com
#### LinkedIn: 
https://www.linkedin.com/in/faz540/

## About Me:
I've always used/preferred "scripting" style languages such as Ruby or JavaScript when writing test automation framworks.
However, using creating a Selenium framework using Java or C# has always been on my To-Do list.

I'm a complete beginner when it comes to using Java, so please feel free to critique this repo - I always appreciate feedback.

## Prerequisite/ Installation:
To clone this repository, you'll obviously need [Git](https://git-scm.com/download/win)
The above assumes you're using Windows.

You must also have [Java Development Kit](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) installed.

Once installed, make sure to set the appropriate "JAVA_HOME" environment variable (This should point to where Java Developer Kit was installed).

Also, make sure to add the "bin" folder inside the JDK folder from above to your "PATH" environment variable too.

Then, download and unzip [Maven](https://maven.apache.org/download.cgi).
Followed by adding the "MAVEN_HOME" environment varaible (This should point to where Maven has been unzipped).

And finally, add the Maven "bin" folder to your "PATH" environment variable too.

## Cloning The Repository:
Clone the repo and using terminal, 'change directory' into the cloned repo.

## Build, Compile and Running the Selenium Tests:
In your terminal, once you're in the "testng-selenium-framework" directory - run the below:

```
mvn test
```

This will build, compile and run the Selenium tests I've outlined in the testNG.xml file.

This is similar to a Ruby RakeFile or a NodeJS Package.json (It's where I've outlined what tests to run, what tests NOT to run and how many threads to use - If I'm running tests in parallel)

Alot more can be added to the XML file, but for now that's all I have in it.