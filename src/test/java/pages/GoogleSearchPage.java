package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.GoogleResultsPage;

public class GoogleSearchPage {
	WebDriver driver = null;
	static By textbox_search = By.name("q");
	static By button_search = By.name("btnK");
	
	public GoogleSearchPage(WebDriver driver) {
		this.driver = driver;
	};
	
	public void open() {
		driver.get("https://google.com");
	};
	
	public void searchFor(String searchQuery) {
		driver.findElement(textbox_search).sendKeys(searchQuery);
		driver.findElement(textbox_search).sendKeys(Keys.RETURN);
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(GoogleResultsPage.text_results));
	};
}