package test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

// Page Objects:
import pages.GoogleSearchPage;

@Listeners(listeners.TestNGListeners.class)
public class GoogleSearch {
	private static WebDriver driver;
	private static ExtentHtmlReporter htmlReporter;
	private static ExtentReports extent;

	@BeforeSuite
	public void setUp() throws InterruptedException {
		htmlReporter = new ExtentHtmlReporter("extent.html");
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
		String projectPath = System.getProperty("user.dir");
		System.setProperty("webdriver.chrome.driver", projectPath + "/drivers/chromedriver.exe");
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability("acceptSslCerts", true);
		caps.setCapability("acceptInsecureCerts", true);
		// If using IE Driver:
		// caps.setCapability("ignoreProtectedModeSettings", true);
		// caps.setCapability("ignoreZoomSetting", true);
		ChromeOptions options = new ChromeOptions();
		options.addArguments("start-maximized");
		driver = new ChromeDriver(options);
		driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
	};
	
	@AfterSuite
	public void tearDown() {
		driver.quit();
		extent.flush();
	};
	
	@BeforeTest
	public void beforeTests() {
		driver.manage().deleteAllCookies();
	};
	
	@AfterTest
	public void afterTests() {
		driver.close();
	};

	
	@Test
	public static void searchForGear4Music() throws Exception {
		ExtentTest test = extent.createTest("Search for 'Gear 4 Music'");
		GoogleSearchPage searchPage = new GoogleSearchPage(driver);
		searchPage.open();
		searchPage.searchFor("Gear 4 Music");
		String currentPageTitle = driver.getTitle();
		Assert.assertEquals(currentPageTitle, "Gear 4 Music - Google Search");
		test.pass("Results were returned.");
	};

	@Test
	public static void searchForGuildWars() throws Exception {
		ExtentTest test = extent.createTest("Search for 'Guild Wars'");
		GoogleSearchPage searchPage = new GoogleSearchPage(driver);
		searchPage.open();
		searchPage.searchFor("Guild Wars");
		String currentPageTitle = driver.getTitle();
		Assert.assertEquals(currentPageTitle, "Guild Wars - Google Search");
		test.pass("Results were returned.");
		test.pass("Results were returned.");
	};
};