package listeners;

import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestNGListeners implements ITestListener, ISuiteListener {
	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub
	};

	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub
	};

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub
	};

	public void onTestFailure(ITestResult result) {
		System.out.println("Test Failed: " + result.getName());
		// Take screenshot?
	};

	public void onTestSkipped(ITestResult result) {
		System.out.println("Test Skipped: " + result.getName());
	};

	public void onTestStart(ITestResult result) {
		// TODO Auto-generated method stub
	};

	public void onTestSuccess(ITestResult result) {
		System.out.println("Test Passed: " + result.getName());
	}

	public void onFinish(ISuite suite) {
		// TODO Auto-generated method stub
		
	}

	public void onStart(ISuite suite) {
		// TODO Auto-generated method stub
		
	};
};